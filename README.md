this script tries to replace every news about sports on tagesschau.de with "brrfz".

prerequisites
-------------
- install firefox
- install [greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)

installation
------------
- go to [tagesschau_sportfrei.js](tagesschau_sportfrei.js)
- click on the icon for "copy file contents" or just copy the file content
- in firefox: greasemonkey -> new user script -> paste
