// ==UserScript==
// @name     tagesschau_sportfrei
// @description tagesschau ohne sport
// @include     https://www.tagesschau.de/*
// @version  1.0.1
// @grant    none
// ==/UserScript==

function replace_text(obj){
	obj.innerHTML = "brrfz";
}

function remove_sports(){
	const re = /sportschau.de\/|\/sport\//;
	const sections = document.querySelectorAll('.listwrapper, .teasergroup, .sectionA');
	for(let s = 0; s < sections.length; ++s){
		const sportticker = sections[s].querySelectorAll('.sportticker');
		if(sportticker.length > 0){
			replace_text(sections[s]);
		}else{
			const teasers = sections[s].querySelectorAll('.teaser__link, .teaser, .list-element');
			for(let t = 0; t < teasers.length; ++t){
				if(re.test(teasers[t].innerHTML)){
					if(window.location.href.startsWith('https://www.tagesschau.de/suche')){
						replace_text(teasers[t]);
					}else{
						replace_text(sections[s]);
						break;
					}
				}
			}
		}
	}
	const modCons = document.getElementsByClassName('modCon');
	for(let m = 0; m < modCons.length; ++m){
		const ressorts = modCons[m].getElementsByClassName('ressort');
		if(ressorts.length > 0 && ressorts[0].innerText == "Sport"){
			replace_text(modCons[m]);
		}
	}
}

remove_sports();
